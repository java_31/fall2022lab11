public class Point {
    public int x;
    public int y;

    public Point(int x, int y){
        this.x = x;
        this.y = y;
    }

    //overriding equals method
    @Override
    public boolean equals(Object other){
        if(!(other instanceof Point)){
            return false;
        }
        Point otherPoint = (Point) other;
        return this.x==otherPoint.x&&this.y==otherPoint.y;
    }
}
