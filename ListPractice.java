import java.util.List;
import java.util.ArrayList;

public class ListPractice{
    public static void main(String [] args) {
        String [] words = new String [] {"hello","HELLO","MY","love","CHEESE"};

        /*Part two */ 
        List <String> wordsList = new ArrayList<String>();
        System.out.println(wordsList.size());

        //demonstrating intial capacity deos not affect size
        List <String> stillEmpty = new ArrayList<String>(50);
        System.out.println(stillEmpty.size());

        //adding into words
        for (String aWord : words) {
            wordsList.add(aWord);
        }
        System.out.println(wordsList.size());

        //Testing overloaded countUpperCase method
        int upperCount = countUpperCase(wordsList);
        System.out.println("Amount of Captial words in array list: " +upperCount);

        //Testing overloader getUpperCase method
        List<String> onlyUpper = getUpperCase(wordsList);
        System.out.println("Only capital words from array are: " +onlyUpper);

        /*Part Four*/
        System.out.println(wordsList.contains("hello"));
        System.out.println(wordsList.contains("potato"));

        /*Part Five*/
        ArrayList<Point> points = new ArrayList<Point>();
        Point p1 = new Point(1,2);
        Point p2 = new Point(2,4);
        Point p3 = new Point(2,2);

        //adding into arraylist points
        points.add(p1);
        points.add(p2);
        points.add(p3);

        // creating fourth point target
        Point target = new Point(2,4);

        //cheching if points contain target
        System.out.println("Does points contain target: " +points.contains(target));
    }


    public static int countUpperCase(List<String> words) {
        int count = 0;
        for (String str : words){
            if (isUpperCase(str)){
                count++;
            }
        }
        return count;   
    }
    //helper method for method countUpperCase
    public static boolean isUpperCase(String s){
        if(s.matches("^[A-Z]*$")){
            return true;
        }
        else{
            return false;
        }
    }


    public static List<String> getUpperCase(List<String> words){
        List<String> upperStrings = new ArrayList<String>();
        for (String string : words) {
            if (isUpperCase(string)){
                    upperStrings.add(string);
            }
        }
        return upperStrings;

    }

}